# CHANGELOG

## 1.0.2
- Changed GPG Key signing to the maven deploy stage to allow `mvn clean install` to continue without error.

## 1.0.1
- Changed text in file `README.md`

## 1.0.0
- Initial Release
- Updated Gitlab file to point to new deployment pipeline
- Enabled merge trains
- Change to not delete source branch.