# Parent POM for Publicly Available Code

## Overview
This parent POM is for all libraries and applications that are open source that will be deployed to Maven Central or OSSRH and maintained by Coarpro LLC.