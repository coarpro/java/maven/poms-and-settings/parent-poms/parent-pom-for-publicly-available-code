<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.coarpro.parentpoms.public</groupId>
  <artifactId>parent-pom-for-publicly-available-code</artifactId>
  <version>1.0.2</version>
  <packaging>pom</packaging>
  <name>Parent POM for publicly available code</name>
  <description>A POM designed to be used as a parent for Java code that will be uploaded to OSSRH and Maven central</description>
  <url>https://www.coarpro.com</url>

  <developers>
    <developer>
      <name>Coarpro LLC</name>
      <email>support@coarpro.com</email>
      <organization>Coarpro LLC</organization>
      <organizationUrl>https://www.coarpro.com</organizationUrl>
    </developer>
  </developers>

  <!--
    If a repository, library, application,
    or project is missing a license file, or if
    the POM file does not specify a license
    then this license will apply. Otherwise
    the license provided at the repository,
    library, application, or project level
    serves as the official license and overrules
    this.
  -->
  <licenses>
    <license>
      <name>Apache License, Version 2.0</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <scm>
    <connection>scm:git:git://gitlab.com/coarpro/libraries/java/poms/parent-pom-for-publicly-available-code.git</connection>
    <developerConnection>scm:git:git://gitlab.com/coarpro/libraries/java/poms/parent-pom-for-publicly-available-code.git</developerConnection>
    <url>https://gitlab.com/coarpro/libraries/java/poms/parent-pom-for-publicly-available-code</url>
  </scm>

 <distributionManagement>
    <snapshotRepository>
      <id>nexus</id>
      <name>Sonatype Nexus Snapshots</name>
      <url>https://s01.oss.sonatype.org/content/repositories/snapshots</url>
    </snapshotRepository>
    <repository>
      <id>nexus</id>
      <name>Nexus Release Repository</name>
      <url>https://s01.oss.sonatype.org/service/local/staging/deploy/maven2</url>
    </repository>
  </distributionManagement>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>17</maven.compiler.source>
    <maven.compiler.target>17</maven.compiler.target>
    <junit.jupiter.version>5.10.0</junit.jupiter.version>
    <mockito.junit.jupiter.version>5.5.0</mockito.junit.jupiter.version>
    <javadoc.plugin.version>3.6.0</javadoc.plugin.version>
    <maven.source.plugin.version>3.3.0</maven.source.plugin.version>
    <maven.gpg.plugin.version>3.1.0</maven.gpg.plugin.version>
    <nexus.staging.plugin.version>1.6.13</nexus.staging.plugin.version>
  </properties>

  <!--
    Required dependencies to be included.
    Doing this prevents from having to manually list
    in child poms and also forces that unit tests are
    run.
  -->
  <dependencies>
    <!--https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-engine-->
    <dependency>
        <groupId>org.junit.jupiter</groupId>
        <artifactId>junit-jupiter-engine</artifactId>
        <version>${junit.jupiter.version}</version>
        <scope>test</scope>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.mockito/mockito-junit-jupiter -->
    <dependency>
        <groupId>org.mockito</groupId>
        <artifactId>mockito-junit-jupiter</artifactId>
        <version>${mockito.junit.jupiter.version}</version>
        <scope>test</scope>
    </dependency>
  </dependencies>

  <build>
    <pluginManagement><!-- lock down plugins versions to avoid using Maven defaults (may be moved to parent pom) -->
      <plugins>
        <!-- clean lifecycle, see https://maven.apache.org/ref/current/maven-core/lifecycles.html#clean_Lifecycle -->
        <plugin>
          <artifactId>maven-clean-plugin</artifactId>
          <version>3.1.0</version>
        </plugin>
        <!-- default lifecycle, jar packaging: see https://maven.apache.org/ref/current/maven-core/default-bindings.html#Plugin_bindings_for_jar_packaging -->
        <plugin>
          <artifactId>maven-resources-plugin</artifactId>
          <version>3.0.2</version>
        </plugin>
        <plugin>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>3.8.0</version>
        </plugin>
        <plugin>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>2.22.1</version>
        </plugin>
        <plugin>
          <artifactId>maven-jar-plugin</artifactId>
          <version>3.0.2</version>
        </plugin>
        <plugin>
          <artifactId>maven-install-plugin</artifactId>
          <version>2.5.2</version>
        </plugin>
        <plugin>
          <artifactId>maven-deploy-plugin</artifactId>
          <version>2.8.2</version>
        </plugin>
        <!-- site lifecycle, see https://maven.apache.org/ref/current/maven-core/lifecycles.html#site_Lifecycle -->
        <plugin>
          <artifactId>maven-site-plugin</artifactId>
          <version>3.7.1</version>
        </plugin>
        <plugin>
          <artifactId>maven-project-info-reports-plugin</artifactId>
          <version>3.0.0</version>
        </plugin>
      <!-- Maven Source Plugin-->
        <plugin>
          <artifactId>maven-source-plugin</artifactId>
          <version>${maven.source.plugin.version}</version>
          <executions>
            <execution>
              <id>attach-source</id>
              <phase>compile</phase>
              <goals>
                <goal>jar-no-fork</goal>
              </goals>
            </execution>
          </executions>
        </plugin>
        <!--Javadoc plugin-->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-javadoc-plugin</artifactId>
          <version>${javadoc.plugin.version}</version>
          <executions>
            <execution>
              <id>attach-javadocs</id>
              <goals>
                <goal>jar</goal>
              </goals>
            </execution>
          </executions>
        </plugin>
        <!--GPG-->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-gpg-plugin</artifactId>
          <version>${maven.gpg.plugin.version}</version>
          <executions>
            <execution>
              <id>sign-artifacts</id>
              <phase>deploy</phase>
              <goals>
                <goal>sign</goal>
              </goals>
              <configuration>
                <passphrase>${env.GPG_KEY_PASSPHRASE}</passphrase>
              </configuration>
            </execution>
          </executions>
        </plugin>
        <!--Checkstyle-->
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-checkstyle-plugin</artifactId>
          <version>3.3.0</version>
          <configuration>
          <!-- Modified from https://github.com/checkstyle/checkstyle/blob/master/src/main/resources/sun_checks.xml -->
          <!-- Adjust checkstyle checks: https://checkstyle.sourceforge.io/checks.html-->
            <configLocation>checkstyle.xml</configLocation>
            <encoding>UTF-8</encoding>
            <consoleOutput>true</consoleOutput>
            <failsOnError>true</failsOnError>
            <linkXRef>false</linkXRef>
          </configuration>
          <executions>
            <execution>
              <id>test</id>
              <phase>test</phase>
              <goals>
                <goal>check</goal>
              </goals>
            </execution>
          </executions>
        </plugin>
        <!--Spotbugs-->
        <plugin>
          <groupId>com.github.spotbugs</groupId>
          <artifactId>spotbugs-maven-plugin</artifactId>
          <version>4.7.2.1</version>
          <executions>
            <execution>
              <id>run-spotbugs</id>
              <phase>test</phase>
              <goals>
                <goal>check</goal>
              </goals>
            </execution>
          </executions>
          <dependencies>
            <!-- overwrite dependency on spotbugs if you want to specify the version of spotbugs -->
            <dependency>
              <groupId>com.github.spotbugs</groupId>
              <artifactId>spotbugs</artifactId>
              <version>4.7.3</version>
            </dependency>
          </dependencies>
        </plugin>
        <plugin>
          <groupId>org.sonatype.plugins</groupId>
          <artifactId>nexus-staging-maven-plugin</artifactId>
          <version>${nexus.staging.plugin.version}</version>
          <extensions>true</extensions>
          <configuration>
            <serverId>nexus</serverId>
            <nexusUrl>https://s01.oss.sonatype.org/</nexusUrl>
            <autoReleaseAfterClose>true</autoReleaseAfterClose>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
    <plugins>
      <!--Maven source plugin-->
      <plugin>
        <artifactId>maven-source-plugin</artifactId>
        <version>${maven.source.plugin.version}</version>
        <executions>
          <execution>
            <id>attach-source</id>
            <phase>compile</phase>
            <goals>
              <goal>jar-no-fork</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <!--Javadoc plugin-->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>${javadoc.plugin.version}</version>
        <executions>
          <execution>
            <id>attach-javadocs</id>
            <goals>
              <goal>jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-gpg-plugin</artifactId>
        <version>${maven.gpg.plugin.version}</version>
      </plugin>
      <plugin>
        <groupId>org.sonatype.plugins</groupId>
        <artifactId>nexus-staging-maven-plugin</artifactId>
        <version>${nexus.staging.plugin.version}</version>
      </plugin>
    </plugins>
  </build>
</project>
